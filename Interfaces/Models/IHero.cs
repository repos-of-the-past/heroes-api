namespace HeroesAPI.Interfaces.Models
{
    public interface IHero
    {
         string Name { get; set; }

         string Description { get; set; }

         bool IsActive { get; set; }
    }
}
