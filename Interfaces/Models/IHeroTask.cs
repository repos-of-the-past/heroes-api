using HeroesAPI.Models;

namespace HeroesAPI.Interfaces.Models
{
    public interface IHeroTask
    {
        string Task { get; set; }

        bool IsCompleted { get; set; }

        bool IsSuccessful { get; set; }

        bool IsDeleted { get; set; }
    }
}
