﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HeroesAPI.Migrations
{
    public partial class AddIsDeletedToHeroTask : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "HeroTasks",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "HeroTasks");
        }
    }
}
