﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HeroesAPI.Migrations
{
    public partial class AddIsCompletedToHeroTask : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsCompleted",
                table: "HeroTasks",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsCompleted",
                table: "HeroTasks");
        }
    }
}
