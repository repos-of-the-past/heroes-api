using Microsoft.EntityFrameworkCore;

using HeroesAPI.Models;

namespace HeroesAPI.Contexts
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<Hero> Heroes { get; set; }

        public DbSet<HeroTask> HeroTasks { get; set; }
    }
}