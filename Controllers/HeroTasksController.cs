using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using HeroesAPI.Contexts;
using HeroesAPI.Models;
using HeroesAPI.Models.HeroTaskModel;
using HeroesAPI.Services;
using System;
using System.Linq;

namespace HeroesAPI.Controllers
{
    [Route("api/hero-tasks")]
    [ApiController]
    public class HeroTasksController : ApiBaseController<HeroTask>
    {
        private DataContext _context;

        private HeroService _heroService;

        public HeroTasksController(DataContext context, HeroService heroService)
        {
            _context = context;
            _heroService = heroService;
        }

        [HttpGet]
        public override async Task<ActionResult> GetAll()
        {
            var heroTasks = await _context.HeroTasks
                .Include(t => t.Hero)
                .ToListAsync();

            return Ok(heroTasks);
        }

        [HttpGet("{id}")]
        public override async Task<ActionResult> Get(int id)
        {
            var heroTask = await Find(id);

            if (heroTask == null)
            {
                return NotFound();
            }

            return Ok(heroTask);
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] HeroTaskPostModel heroTask)
        {
            int heroId = heroTask.HeroId;
            var hero = await _heroService.FindHero(heroId);

            if (hero == null)
            {
                return NoRecordFound("heroId", heroId.ToString());
            }

            var heroTaskObj = new HeroTask
            {
                Task = heroTask.Task,
                Hero = hero,
                CreatedAt = DateTime.Now
            };

            _context.HeroTasks.Add(heroTaskObj);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(Get), new { id = heroTaskObj.Id }, heroTaskObj);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] HeroTaskPutModel heroTask)
        {
            int heroId = heroTask.HeroId;
            var hero = await _heroService.FindHero(heroId);

            if (id != heroTask.Id)
            {
                return NotFound();
            }
            
            if (hero == null)
            {
                return NoRecordFound("heroId", heroId.ToString());
            }

            var heroTaskObj = new HeroTask
            {
                Id = heroTask.Id,
                Task = heroTask.Task,
                IsCompleted = heroTask.IsCompleted,
                IsSuccessful = heroTask.IsSuccessful,
                Hero = hero,
                UpdatedAt = DateTime.Now
            };

            _context.Entry(heroTaskObj).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public override async Task<IActionResult> Delete(int id)
        {
            var heroTask = await Find(id);

            if (heroTask == null)
            {
                return NotFound();
            }

            heroTask.IsCompleted = false;
            heroTask.IsSuccessful = false;
            heroTask.IsDeleted = true;
            heroTask.UpdatedAt = DateTime.Now;
            _context.Entry(heroTask).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private Task<HeroTask> Find(int id)
        {
            return _context.HeroTasks
                .Include(t => t.Hero)
                .Where(t => t.Id == id)
                .SingleAsync();
        }
    }
}