using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace HeroesAPI.Controllers
{
    public abstract class ApiBaseController<T> : ControllerBase
    {
        public abstract Task<ActionResult> GetAll();

        public abstract Task<ActionResult> Get(int id);

        public virtual Task<ActionResult> Post(T obj) => throw new NotImplementedException();

        public virtual Task<IActionResult> Put(int id, T obj) => throw new NotImplementedException();

        public abstract Task<IActionResult> Delete(int id);

        public virtual ActionResult NoRecordFound(string param, string value)
        {
            return NotFound(new {
                message = $"No record found with {param} {value}"
            });
        }
    }
}
