using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using HeroesAPI.Contexts;
using HeroesAPI.Models;
using HeroesAPI.Models.HeroModel;
using HeroesAPI.Services;
using System.Collections.Generic;

namespace HeroesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HeroesController : ApiBaseController<Hero>
    {
        private DataContext _context;

        private HeroService _heroService;

        public HeroesController(DataContext context, HeroService heroService)
        {
            _context = context;
            _heroService = heroService;
        }

        [HttpGet]
        public override async Task<ActionResult> GetAll()
        {
            var heroes = await _context.Heroes.ToListAsync();

            return Ok(heroes);
        }

        [HttpGet("{id}")]
        public override async Task<ActionResult> Get(int id)
        {
            var hero = await _heroService.FindHero(id);

            if (hero == null)
            {
                return NotFound();
            }

            return Ok(hero);
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] HeroPostModel hero)
        {
            var heroObj = new Hero
            {
                Name = hero.Name,
                Description = hero.Description,
                IsActive = hero.IsActive
            };

            _context.Heroes.Add(heroObj);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(Get), new { id = heroObj.Id }, heroObj);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] HeroPutModel hero)
        {
            if (id != hero.Id)
            {
                return NotFound();
            }

            var heroObj = new Hero
            {
                Id = hero.Id,
                Name = hero.Name,
                Description = hero.Description,
                IsActive = hero.IsActive
            };

            _context.Entry(heroObj).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public override async Task<IActionResult> Delete(int id)
        {
            var hero = await _heroService.FindHero(id);

            if (hero == null)
            {
                return NotFound();
            }

            hero.IsActive = false;
            _context.Entry(hero).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpGet]
        [Route("top")]
        public async Task<ActionResult> GetTop()
        {
            var heroes = await _context.Heroes
                .Where(h => h.IsActive == true)
                .Select(h => new
                {
                    Hero = h,
                    HeroTaskCount = _context.HeroTasks
                        .Where(t => t.Hero.Id == h.Id)
                        .Where(t => t.IsCompleted == true &&
                            t.IsCompleted == true &&
                            t.IsDeleted == false)
                        .Count()
                })
                .OrderBy(h => h.HeroTaskCount)
                .ToListAsync();

            return Ok(heroes);
        }
    }
}
