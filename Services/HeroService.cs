using System.Threading.Tasks;

using HeroesAPI.Contexts;
using HeroesAPI.Models;

namespace HeroesAPI.Services
{
    public class HeroService
    {
        private DataContext _context;

        public HeroService(DataContext context)
        {
            _context = context;
        }

        public Task<Hero> FindHero(int id)
        {
            return _context.Heroes.FindAsync(id);
        }
    }
}