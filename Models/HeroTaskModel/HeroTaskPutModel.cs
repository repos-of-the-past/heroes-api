using System;
using System.ComponentModel.DataAnnotations;

using HeroesAPI.Interfaces.Models;

namespace HeroesAPI.Models.HeroTaskModel
{
    public class HeroTaskPutModel : IHeroTask
    {
        public int Id { get; set; }

        [Required]
        public string Task { get; set; }

        public bool IsCompleted { get; set; }

        public bool IsSuccessful { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime UpdatedAt { get; set; }

        [Required]
        public int HeroId { get; set; }
    }
}
