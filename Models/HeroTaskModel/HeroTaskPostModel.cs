using System;
using System.ComponentModel.DataAnnotations;

using HeroesAPI.Interfaces.Models;

namespace HeroesAPI.Models.HeroTaskModel
{
    public class HeroTaskPostModel : IHeroTask
    {
        [Required]
        public string Task { get; set; }

        public bool IsCompleted { get; set; } = false;

        public bool IsSuccessful { get; set; } = false;

        public bool IsDeleted { get; set; } = false;

        public DateTime CreatedAt { get; set; }

        [Required]
        public int HeroId { get; set; }
    }
}
