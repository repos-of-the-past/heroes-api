using System;

using HeroesAPI.Interfaces.Models;

namespace HeroesAPI.Models
{
    public class HeroTask : IHeroTask
    {
        public int Id { get; set; }

        public string Task { get; set; }

        public bool IsCompleted { get; set; }

        public bool IsSuccessful { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public Hero Hero { get; set; }
    }
}
