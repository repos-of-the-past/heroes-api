using System.ComponentModel.DataAnnotations;

using HeroesAPI.Interfaces.Models;

namespace HeroesAPI.Models.HeroModel
{
    public class HeroPostModel : IHero
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public bool IsActive { get; set; } = true;
    }
}
