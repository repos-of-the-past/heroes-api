using System.ComponentModel.DataAnnotations;

using HeroesAPI.Interfaces.Models;

namespace HeroesAPI.Models.HeroModel
{
    public class HeroPutModel : IHero
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public bool IsActive { get; set; } = true;
    }
}
